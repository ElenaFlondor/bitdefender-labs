#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

//stores the number of files found in the file tree of a given directory
long RfileCounter = 0;

void getFileInfo(void) {}

long getFilesInCurrDir(const char* sDir)
{
    char sPath[2048];
    sprintf(sPath, "%s//*.*", sDir);

    HANDLE hFile;
    WIN32_FIND_DATA FindData;
    long fileCounter = 0;

    // Find the first file

    hFile = FindFirstFile(sPath, &FindData);

    if (hFile == INVALID_HANDLE_VALUE)
    {
        printf("Invalid file handle.");
        return (-1);
    }

    // Look for more
    fileCounter++;

    while (FindNextFile(hFile, &FindData))
    {
        //printf("%s\n", FindData.cFileName);

        //FindNextFile always returns ".." on the first run
        if(strcmp(FindData.cFileName,"..") != 0)
        {
            fileCounter++;
            getFileInfo(); //TO DO
        }
    }

    // Close the file handle

    FindClose(hFile);
    return fileCounter;

}

int getCurrDirFileTree(const char* sDir)
{
    char sPath[2048];
    HANDLE hFile;
    WIN32_FIND_DATA FindData;

    sprintf(sPath, "%s\\*.*", sDir);

    // Find the first file

    hFile = FindFirstFile(sPath, &FindData);

    if (hFile == INVALID_HANDLE_VALUE)
    {
        printf("%s\n","Invalid file handle.");
        return (-1);
    }

    // Look for more

    RfileCounter++;

    while (FindNextFile(hFile, &FindData))
    {
        //FindNextFile always returns ".." on the first run
        if((strcmp(FindData.cFileName, ".") != 0) && (strcmp(FindData.cFileName,"..") != 0))
        {
            sprintf(sPath, "%s\\%s", sDir, FindData.cFileName);

            if(FindData.dwFileAttributes &FILE_ATTRIBUTE_DIRECTORY)
            {
                getCurrDirFileTree(sPath);
            }
            else
            {

                RfileCounter++;
                getFileInfo(); //TO DO; also, check if the file is of the types we want to scan
            }
        }
    }

    // Close the file handle

    FindClose(hFile);
}

int main(int argc, char* argv[])
{
    /*TO DO:
        parse the input
        if dealing with a directory, use source directory path as argument for the two functions below
    */

    //if input was of the form "-d  <directory_path>"
    printf("%ld",getFilesInCurrDir("C:\\Users"));

    //if input was of the form "-dr  <directory_path>"
    getCurrDirFileTree("C:\\Users");
    return 0;
}

